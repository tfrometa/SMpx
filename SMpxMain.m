function varargout = SMpxMain(varargin)

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SMpxMain_OpeningFcn, ...
                   'gui_OutputFcn',  @SMpxMain_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SMpxMain is made visible.
function SMpxMain_OpeningFcn(hObject, eventdata, handles, varargin)

handles.output = hObject;


guidata(hObject, handles);


function varargout = SMpxMain_OutputFcn(hObject, eventdata, handles) 


varargout{1} = handles.output;

function SMpxMod_Callback(hObject, eventdata, handles)
    SMpxMod

function SMpxCal_Callback(hObject, eventdata, handles)
    SMpxCal

