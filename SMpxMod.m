function varargout = SMpxMod(varargin)
%
% This program models stochastic data (x;y) to the SMp(x/y;PXmin,Xmax,ML,p1,p2,MaxSMp, a
% a pure-probabilistic distribution. When SMp(x) is used for probabilities
% fucntion of varible y, instead x; all SMp parameters should be introduced
% while while when is used as SMp(x), the parameter Max is calculated according 
% to type of variable (discrete or continuous), and based on probabilistic 
% conditions of the SMp(x)   


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SMpxMod_OpeningFcn, ...
                   'gui_OutputFcn',  @SMpxMod_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function SMpxMod_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = SMpxMod_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%% Input for tumor %%%%%%%%%%%%%%%%%%%%%%%%

function typefix_Callback(hObject, eventdata, handles)                     % Type of fix, SMp(y) or SMp(x)
    global fix 
    item=get(hObject,'Value'); 
    if item==1
        fix=1;                                                             % fix=1 for SMp in its role of SMp(y)
        set(handles.rvariable,'Visible','Off');
        set(handles.variable,'Visible','Off');
        set(handles.tMax,'Visible','On');
        set(handles.MaxSMp,'Visible','On');
        set(handles.cMax,'Visible','On');
    elseif item==2
        fix=2;                                                             % fix=2  for SMp in its role of SMp(x)
        set(handles.rvariable,'Visible','On');
        set(handles.variable,'Visible','On');
        set(handles.tMax,'Visible','Off');
        set(handles.MaxSMp,'Visible','Off');
        set(handles.cMax,'Visible','Off');
    end                                                           

function typefix_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function variable_Callback(hObject, eventdata, handles)                    % Type of variable, Continuous or Discrete 
    global tvar 
    
    item=get(hObject,'Value'); 
    if item==1
        tvar='c';                                               
    elseif item==2
        tvar='d';    
    end 
function variable_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Xlabel_Callback(hObject, eventdata, handles)
 global Xlabel
 Xlabel=get(hObject,'String'); 
function Xlabel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Ylabel_Callback(hObject, eventdata, handles)
 global Ylabel
 Ylabel=get(hObject,'String'); 
function Ylabel_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function PXmin_Callback(hObject, eventdata, handles)                       % PXmin 
    global PXmin 
    PXmin=str2double(get(hObject,'String'));       
function PXmin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Xmax_Callback(hObject, eventdata, handles)                        % Xmax 
    global Xmax 
    Xmax=str2double(get(hObject,'String'));
function Xmax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ML_Callback(hObject, eventdata, handles)                          % ML
    global ML 
    ML=str2double(get(hObject,'String'));
function ML_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p1_Callback(hObject, eventdata, handles)                          % p1
    global p1 
    p1=str2double(get(hObject,'String'));
function p1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p2_Callback(hObject, eventdata, handles)                          % p2
    global p2 
    p2=str2double(get(hObject,'String'));
function p2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function MaxSMp_Callback(hObject, eventdata, handles)                      % Max
    global MaxSMp
    MaxSMp=str2double(get(hObject,'String'));
function MaxSMp_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function DataXY_Callback(hObject, eventdata, handles)                      % For introducing data (x;y)
  
    global xdata ydata
    
    x = inputdlg('Enter space-separated numbers:',...
             'Values of x', [1 50]);
    xdata = str2num(x{:}); 
    y = inputdlg('Enter space-separated numbers:',...
             'Values of y', [1 50]);
    ydata = str2num(y{:}); 
    
    cla      
    pData=plot(xdata,ydata); 
    hold all
    set(pData,'Color','red','LineStyle','none','Marker','x');    
    
    set(handles.Fit,'Visible','On');
    set(handles.DataXY,'Visible','Off');
    
   

function Fit_Callback(hObject, eventdata, handles)                         % Function for botton of Fit
    global MaxSMp fix  
    
   ploter 
      
    if fix==1;                                                             % For SMp(x)
        set(handles.MaxSMp,'Visible','On');
        set(handles.cMax,'Visible','On'); 
    elseif fix==2
        set(handles.tMax,'Visible','On');
        set(handles.MaxSMp,'Visible','On','BackgroundColor','Green','String',num2str(MaxSMp,3));
        set(handles.cMax,'Visible','On','BackgroundColor','Green','String','........... Calculated by the probabilistic conditions of the SMp(x)');        
    end
    
function ploter                                                            % Function for plotting the data (x;y) and the fitted SMp(x/y)
   
    global  Xlabel Ylabel PXmin Xmax ML p1 p2 MaxSMp fix tvar xdata 
    
    Xmin=cXmin(PXmin);
    deltax=(Xmax-Xmin)/100;
    xSMp=0:deltax:round(Xmax+deltax);
    if fix==2
       
        if tvar=='c'            
            cMaxSMp=cMaxSMpvc(PXmin,Xmax,ML,p1,p2);
        elseif tvar=='d'
            cMaxSMp=cMaxSMpvd(xdata,PXmin,Xmax,ML,p1,p2);
        end       
            
        MaxSMp=cMaxSMp;
    end    
    for i=1:numel(xSMp)
        ySMp(i)=SMp(xSMp(i),PXmin,Xmax,ML,p1,p2,MaxSMp);     
    end 
    pSMp=plot(xSMp,ySMp);
    set(pSMp,'Color','blue','LineWidth',2); 
    xlabel(Xlabel); ylabel(Ylabel);
    legend('Data','SMp(x)');
   
    
function cMaxSMpvc=cMaxSMpvc(PXmin,Xmax,ML,p1,p2)                          % Function for calcualting Max for random continuous variable 
    Xmin=cXmin(PXmin); 
    cte1=(ML-PXmin)/(p1+1); cte2=(ML-Xmax)/(p2+1);
    intg1=cte1* (power((ML-PXmin)/(ML-PXmin),p1+1)-power((Xmin-PXmin)/(ML-PXmin),p1+1));
    intg2=cte2* (power((Xmax-Xmax)/(Xmax-ML),p2+1)-power((Xmax-ML)/(Xmax-ML),p2+1));
    intcMaxSMp=intg1+intg2;   
    cMaxSMpvc=1/intcMaxSMp;
        
 function cMaxSMpvd=cMaxSMpvd(xdata,PXmin,Xmax,ML,p1,p2)                   % Function for calcualting Max for random discrete variable 
      for i=1:numel(xdata)
            ydataSMp(i)=SMp(xdata(i),PXmin,Xmax,ML,p1,p2,1);
      end    
        cMaxSMpvd=1/(sum(ydataSMp));
        
  
   
function SMp=SMp(x,PXmin,Xmax,ML,p1,p2,MaxSMp)                             % Function SMp(x)
   
    Xmin=cXmin(PXmin);
    
   if x<Xmin
       SMp=0; 
   end 
   if ((x>=Xmin) && (x<=ML)) 
       SMp= power((x-PXmin)/(ML-PXmin),p1)*MaxSMp;
   end
   if ((x>ML) && (x<=Xmax)) 
       SMp= power((Xmax-x)/(Xmax-ML),p2)*MaxSMp;
   end
   if x>Xmax
       SMp=0; 
   end
       
   
function cXmin=cXmin(PXmin)                                                % Function for calculating of Xmin from PXmin

    if PXmin<0
        cXmin=0;
    else 
        cXmin=PXmin;  
    end  
   
    
    

    


















































