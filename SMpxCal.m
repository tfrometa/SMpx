function varargout = SMpxCal(varargin)
%
% This program calculates probabilities using the SMp(x/y;PXmin,Xmax,ML,p1,p2,MaxSMp, a
%    


% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SMpxCal_OpeningFcn, ...
                   'gui_OutputFcn',  @SMpxCal_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function SMpxCal_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = SMpxCal_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%%%%%%%%%%%%%%%%%%%%% Input for tumor %%%%%%%%%%%%%%%%%%%%%%%%

function PXmin_Callback(hObject, eventdata, handles)                       % PXmin 
    global PXmin 
    PXmin=str2double(get(hObject,'String'));       
function PXmin_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Xmax_Callback(hObject, eventdata, handles)                        % Xmax 
    global Xmax 
    Xmax=str2double(get(hObject,'String'));
function Xmax_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function ML_Callback(hObject, eventdata, handles)                          % ML
    global ML 
    ML=str2double(get(hObject,'String'));
function ML_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p1_Callback(hObject, eventdata, handles)                          % p1
    global p1 
    p1=str2double(get(hObject,'String'));
function p1_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function p2_Callback(hObject, eventdata, handles)                          % p2
    global p2 
    p2=str2double(get(hObject,'String'));
function p2_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function MaxSMp_Callback(hObject, eventdata, handles)                      % Max
    global MaxSMp
    MaxSMp=str2double(get(hObject,'String'));
function MaxSMp_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function Prob_Callback(hObject, eventdata, handles)                        % Type of probability 
    global prob 
    item=get(hObject,'Value'); 
    if item==1
        prob=1;                                                            % prob=1 for probability X <=a 
        set(handles.b,'Visible','Off');
        set(handles.mvalueb,'Visible','Off');
        set(handles.a,'Visible','On');
        set(handles.mvaluea,'Visible','On');
    elseif item==2
        prob=2;                                                             % prob=2  for probability a <= X <= b 
        set(handles.b,'Visible','On');
        set(handles.mvalueb,'Visible','On');
        set(handles.a,'Visible','On');
        set(handles.mvaluea,'Visible','On');
    end                                                           

function Prob_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function a_Callback(hObject, eventdata, handles)
    global a
    a=str2double(get(hObject,'String'));
function a_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function b_Callback(hObject, eventdata, handles)
    global b
    b=str2double(get(hObject,'String'));
function b_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Calculate_Callback(hObject, eventdata, handles)                   % Function for botton of Calculate
    global prob a b PXmin Xmax ML p1 p2 MaxSMp 
    
   if prob==1;                                                            
       
        result1=SMpCDF(a,PXmin,Xmax,ML,p1,p2,MaxSMp); 
        result2=1-result1;
        set(handles.mresult1,'Visible','On');
        set(handles.mresult1,'String','P(X<=a):');
        set(handles.result1,'Visible','On');
        set(handles.result1,'String', num2str(result1,3));
        set(handles.mresult2,'Visible','On');
        set(handles.mresult2,'String','P(X>a):');
        set(handles.result2,'Visible','On');
        set(handles.result2,'String', num2str(result2,3));
        
    elseif prob==2  
        
        result1=SMpCDF(b,PXmin,Xmax,ML,p1,p2,MaxSMp)-SMpCDF(a,PXmin,Xmax,ML,p1,p2,MaxSMp); 
        result2=1-result1;
        set(handles.mresult1,'Visible','On');
        set(handles.mresult1,'String','P(a<= X <= b):');
        set(handles.result1,'Visible','On');
        set(handles.result1,'String', num2str(result1,3));
        set(handles.mresult2,'Visible','On');
        set(handles.mresult2,'String','P(X<a or X>b):');
        set(handles.result2,'Visible','On');
        set(handles.result2,'String', num2str(result2,3));
    end                         
    
   
      
function SMpCDF=SMpCDF(x,PXmin,Xmax,ML,p1,p2,MaxSMp)                       % Function for calculating the SMp CDF 
   
    
   Xmin=cXmin(PXmin); 
   cte1=(ML-PXmin)/(p1+1); cte2=(ML-Xmax)/(p2+1);
   intg1=MaxSMp*cte1* (power((ML-PXmin)/(ML-PXmin),p1+1)-power((Xmin-PXmin)/(ML-PXmin),p1+1));
   
   if x<Xmin
       SMpCDF=0; 
   else
       if x<=ML 
           SMpCDF= MaxSMp*cte1* (power((x-PXmin)/(ML-PXmin),p1+1)-power((Xmin-PXmin)/(ML-PXmin),p1+1));
       end
       if x>ML 
           SMpCDF= intg1+ MaxSMp*cte2* (power((Xmax-x)/(Xmax-ML),p2+1)-power((Xmax-ML)/(Xmax-ML),p2+1));
       end
   end   
  
   
     
   
function cXmin=cXmin(PXmin)                                                % Function for calculating of Xmin from PXmin

    if PXmin<0
        cXmin=0;
    else 
        cXmin=PXmin;  
    end  
   
    
    

    





















































